<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\UserStatistic;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class UserStatisticsSeeder extends Seeder
{
    public function run()
    {
        $users = User::all();

        foreach ($users as $user) {
            UserStatistic::firstOrCreate([
                'user_id' => $user->id,
                'leads' => random_int(10,500),
                'sales' => random_int(1,100),
            ]);
        }
    }
}
