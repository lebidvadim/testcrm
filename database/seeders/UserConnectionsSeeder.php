<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\UserConnection;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class UserConnectionsSeeder extends Seeder
{
    public function run()
    {
        $teamLeader1 = User::find(2);
        $role = Role::where('name', 'baer')->first();
        $baers = User::role($role)->get();
        $baers = $baers->take(3);

        foreach ($baers as $baer) {
            UserConnection::firstOrCreate([
                'team_id' => $teamLeader1->id,
                'baer_id' => $baer->id,
            ]);
        }
    }
}
