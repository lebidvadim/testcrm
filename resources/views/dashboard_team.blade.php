<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white dark:bg-gray-800 overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900 dark:text-gray-100">
                    <h1>Моя статистика</h1>
                    <br /><hr><br />
                    <div>Leads: {{ $user->statistics->leads }} | Sales: {{ $user->statistics->sales }}</div>
                    <br /><hr><br />
                    <h1>Cтатистика баеров</h1>
                    <br /><hr><br />
                    @if($user->connections->count())
                        @foreach($user->connections as $connection)
                            <div>Name: {{ $connection->baer->name }} | Role: {{ $connection->baer->roles[0]->name }} | Leads: {{ $connection->baer->statistics->leads }} | Sales: {{ $connection->baer->statistics->sales }}</div>
                        @endforeach
                    @else
                        У вас нет прикрепленных Баеров.
                    @endif
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
