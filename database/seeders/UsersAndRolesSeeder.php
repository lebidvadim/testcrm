<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\User;
use Spatie\Permission\Models\Role;

class UsersAndRolesSeeder extends Seeder
{
    public function run()
    {
        $adminUser = User::create([
            'name' => 'Admin',
            'email' => 'admin@example.com',
            'password' => bcrypt('password'),
        ]);

        $teamLeadUser1 = User::create([
            'name' => 'Team Lead 1',
            'email' => 'teamlead1@example.com',
            'password' => bcrypt('password'),
        ]);

        $teamLeadUser2 = User::create([
            'name' => 'Team Lead 2',
            'email' => 'teamlead2@example.com',
            'password' => bcrypt('password'),
        ]);

        $teamLeadUser3 = User::create([
            'name' => 'Team Lead 3',
            'email' => 'teamlead3@example.com',
            'password' => bcrypt('password'),
        ]);

        $bearerUser1 = User::create([
            'name' => 'Bearer 1',
            'email' => 'bearer1@example.com',
            'password' => bcrypt('password'),
        ]);

        $bearerUser2 = User::create([
            'name' => 'Bearer 2',
            'email' => 'bearer2@example.com',
            'password' => bcrypt('password'),
        ]);

        $bearerUser3 = User::create([
            'name' => 'Bearer 3',
            'email' => 'bearer3@example.com',
            'password' => bcrypt('password'),
        ]);

        $bearerUser4 = User::create([
            'name' => 'Bearer 4',
            'email' => 'bearer4@example.com',
            'password' => bcrypt('password'),
        ]);

        // Назначение ролей пользователям
        $adminRole = Role::findByName('admin');
        $teamLeadRole = Role::findByName('team-leader');
        $bearerRole = Role::findByName('baer');

        $adminUser->assignRole($adminRole);
        $teamLeadUser1->assignRole($teamLeadRole);
        $teamLeadUser2->assignRole($teamLeadRole);
        $teamLeadUser3->assignRole($teamLeadRole);
        $bearerUser1->assignRole($bearerRole);
        $bearerUser2->assignRole($bearerRole);
        $bearerUser3->assignRole($bearerRole);
        $bearerUser4->assignRole($bearerRole);
    }
}
