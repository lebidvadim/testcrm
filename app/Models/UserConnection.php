<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;

class UserConnection extends Model
{
    use HasFactory;
    protected $fillable = [
        'team_id',
        'baer_id',
    ];
    public function baer(): BelongsTo
    {
        return $this->belongsTo(User::class, 'baer_id');
    }
}
