<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class StatisticsController extends Controller
{
    public function __invoke(Request $request)
    {
        $user = Auth::user();
        $data['user'] = $user;
        if($user->hasRole('admin')) {
            $data['user'] = User::where('id', '!=', $user->id)->get();
            $view = view('dashboard', $data);
        }
        elseif($user->hasRole('team-leader')) {
            $view = view('dashboard_team', $data);
        }
        elseif($user->hasRole('baer')) {
            $view = view('dashboard_baer', $data);
        }

        return $view;
    }
}
