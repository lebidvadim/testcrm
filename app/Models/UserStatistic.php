<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class UserStatistic extends Model
{
    use HasFactory;
    protected $fillable = [
        'user_id',
        'leads',
        'sales',
    ];
    public function userId(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
