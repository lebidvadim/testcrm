<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up()
    {
        Schema::create('user_connections', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('team_id')->unsigned();
            $table->bigInteger('baer_id')->unsigned();
            $table->timestamps();

            $table->foreign('team_id')->references('id')->on('users')->cascadeOnDelete();
            $table->foreign('baer_id')->references('id')->on('users')->cascadeOnDelete();
        });

        \Illuminate\Support\Facades\Artisan::call('db:seed',['--class' => 'UserConnectionsSeeder']);
    }
    public function down()
    {
        Schema::dropIfExists('user_connections');
    }
};
